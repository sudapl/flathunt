const idealista = require("./parsers/idealista")
const fotocasa = require("./parsers/fotocasa")
const UnknownUrlError = require("./errors/UnknownUrlError")

/**
 * Parse the URL using the correct parser
 *
 * @async
 * @param {string} url Apartment URL
 */
module.exports = async (url) => {
    const isIdealista = await idealista.validate(url)
    const isFotocasa = await fotocasa.validate(url)

    let data
    if (isIdealista) {
        const body = await idealista.fetch(url)
        data = await idealista.parse(body)
        data.url = isIdealista
    } else if (isFotocasa) {
        const body = await fotocasa.fetch(url)
        data = await fotocasa.parse(body)
        data.url = isFotocasa
    } else {
        throw new UnknownUrlError()
    }
    return data
}