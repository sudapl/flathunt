const test = require("ava")
const fs = require("fs")
const parser = require("./fotocasa")

const NotFoundError = require("../errors/NotFoundError")

test("validates the URL", async t => {
  let result = await parser.validate(
    "https://www.fotocasa.es/es/comprar/vivienda/valencia-capital/aire-acondicionado-terraza-ascensor/148022168/d"
  )
  t.is(
    result,
    "https://www.fotocasa.es/es/comprar/vivienda/valencia-capital/aire-acondicionado-terraza-ascensor/148022168/d"
  )
  result = await parser.validate(
    "http://www.fotocasa.es/Building/Building_Detail.aspx?ai=150140983&opi=11&cu=ES-ES&tti=1"
  )
  t.is(result, "http://www.fotocasa.es/Building/Building_Detail.aspx?ai=150140983&opi=11&cu=ES-ES&tti=1")
})

test("parses the body", async t => {
  const body = fs.readFileSync("test/fotocasa.html")

  let result = await parser.parse(body)
  t.is(result.name, "Piso en Ciutat Jardí, Algirós, Valencia Capital")
  t.is(result.price, 165000)
  t.is(result.m2, 98)
  t.is(result.bedrooms, 3)
  t.is(result.bathrooms, 1)
  t.is(result.floor, 10)
  t.is(result.phoneNumber, "960 968 337")
  t.is(result.photos.length, 28)
  t.is(result.photos[0], "https://d.inmofactory.com/1/85060/18213325/260242732.jpg")
})

test.skip("throws exception when not found", async t => {
  const err = await t.throwsAsync(async () => {
    return parser.fetch("https://www.idealista.com/inmueble/39912242/")
  }, NotFoundError)
})
