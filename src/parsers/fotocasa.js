const got = require("got")
const cheerio = require("cheerio")
const vm = require("vm")

const initialCookie =
  "AMCVS_05FF6243578784B37F000101%40AdobeOrg=1; AMCV_05FF6243578784B37F000101%40AdobeOrg=-1303530583%7CMCIDTS%7C17964%7CMCMID%7C85340459610784010737081028736981234656%7CMCAID%7CNONE%7CMCOPTOUT-1552093489s%7CNONE%7CvVersion%7C3.3.0; s_cc=true; euconsent=BOdHZLQOdHZLQCBAABESCJ-AAAAll7_______9______5uz_Ov_v_f__33e8__9v_l_7_-___u_-33d4-_1vf99yfm1-7ftr3tp_87ues2_Xur__59__3z3_9phPrsks5A; cu=es-es; usunico=09/03/2019:0-09962139; utag_main=v_id:01695f8d4a2b000ac2209cfb6c6100079001b07100fb8$_sn:1$_ss:0$_pn:2%3Bexp-session$_st:1552088137169$ses_id:1552086288939%3Bexp-session$vapi_domain:fotocasa.es$appnexus_sync_session:1552086288939%3Bexp-session; s_sq=schibstedspainrefotocasawebprod%3D%2526c.%2526a.%2526activitymap.%2526page%253Ddetail%2526link%253DLeer%252520descripci%2525C3%2525B3n%252520completa%2526region%253DApp%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253Ddetail%2526pidt%253D1%2526oid%253Dfunctiondr%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DA"
const userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:62.0) Gecko/20100101 Firefox/62.0"

module.exports = {
  validate: async url => {
    const regex1 = /(https:\/\/www\.fotocasa\.es\/)(es\/)(comprar\/)([\w\-]+\/)([\w\-]+\/)([\w\-]+\/)(\d+)\/d/
    const regex2 = /(http:\/\/www\.fotocasa\.es\/)(\w+\/)(.*)/
    const matches = url.match(regex1) || url.match(regex2)

    if (matches && matches.length) {
      return matches[0]
    }
  },

  /**
   * Fetch the Fotocasa URL
   *
   * @returns {Promise}
   */
  fetch: async url => {
    let body

    try {
      let response = await got(url, {
        headers: { "User-Agent": userAgent, Cookie: initialCookie }
      })
      return response.body
    } catch (error) {
      if (error.statusCode == 404) {
        throw new NotFoundError()
      }
      throw error
    }
  },

  parse: async body => {
    const $ = cheerio.load(body)

    const initialProps = $("script")
      .eq(5)
      .contents()
      .text()
      .replace(/window\./g, ";")

    const script = vm.createScript(initialProps, { displayErrors: true })
    const context = vm.createContext()
    script.runInContext(context)

    const props = context.__INITIAL_PROPS__.realEstate

    const name = `${context.__INITIAL_PROPS__.propertyTitle} ${
        props.address.neighborhood}, ${props.address.district},${
        props.address.city}`
    const price = props.price
    const m2 = props.features.surface
    const bedrooms = props.features.rooms
    const bathrooms = props.features.bathrooms
    const floor = props.features.floor
    const phone = props.phone
    const phoneNumber = `${phone.substr(0, 3)} ${phone.substr(3, 3)} ${phone.substr(6, 3)}`

    const photos = props.multimedia.map(m => m.src)

    return {
      name,
      price,
      m2,
      bedrooms,
      bathrooms,
      floor,
      phoneNumber,
      photos
    }
  }
}
