const test = require("ava")
const fs = require("fs")
const parser = require("./idealista")

const NotFoundError = require("../errors/NotFoundError")

test("validates the URL", async t => {
  let result = await parser.validate(
    "https://www.idealista.com/inmueble/84541213/"
  )
  t.is(result, "https://www.idealista.com/inmueble/84541213/")
  const longUrl =
    "https://www.idealista.com/inmueble/83877235/?adId=83877235&ident=u%2BPAHwQjPIMt318ar4yqV3EaT8pHM7lPeNCR2AJi40oopxyZOsez4ts%2BJKQjifW%2BJjETTyHuNHc%3D&xts=352991&xtor=EPR-226-%5Bfavorites_add_photo_20190305%5D-20190305-%5Binmueble%5D-%5B%5D-20190305200203"
  result = await parser.validate(longUrl)
  t.is(result, "https://www.idealista.com/inmueble/83877235/")
})

test.skip("fetches the URL", async t => {
  let result = await parser.fetch(
    "https://www.idealista.com/inmueble/84541213/"
  )

  fs.writeFileSync("test/idealista.html", result.body)
})

test("parses the body", async t => {
  const body = fs.readFileSync("test/idealista.html")

  let result = await parser.parse(body)
  t.is(result.name, "La Creu del Grau, Camins al Grau, Valencia")
  t.is(result.price, 148000)
  t.is(result.m2, 101)
  t.is(result.bedrooms, 4)
  t.is(result.floor, 4)
  t.is(result.phoneNumber, "960 965 239")
  t.is(result.photos.length, 37)
  t.is(
    result.photos[0],
    "https://img3.idealista.com/blur/WEB_DETAIL_TOP-XL-P/0/id.pro.es.image.master/aa/d7/40/641413684.jpg"
  )
})

test("throws exception when not found", async t => {
  const err = await t.throwsAsync(async () => {
    return parser.fetch("https://www.idealista.com/inmueble/39912242/")
  }, NotFoundError)
})
