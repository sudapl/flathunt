const got = require("got")
const cheerio = require("cheerio")
const NotFoundError = require("../errors/NotFoundError")
const ForbiddenError = require("../errors/ForbiddenError")

const initialCookie = `IQ_userIdCookie=ES00118805628; has_js=1; listingGalleryBoostEnabled=false; _pxhd=5a1d64c79edbf5343b65af3376fc49d4879b48033ebecf70160e1bb8cf7eee86:c912e491-a05e-11ea-8960-c3332df5bdc9; _hjid=4123291a-6e63-418f-a036-f7f1f009bf25; xtan352991=1-118805628; xtant352991=1; TestIfCookie=ok; TestIfCookieP=ok; atidvisitor=%7B%22name%22%3A%22atidvisitor%22%2C%22val%22%3A%7B%22vrn%22%3A%22-582065-%22%2C%22at%22%3A%22ES00118805628%22%2C%22ac%22%3A%221%22%7D%2C%22options%22%3A%7B%22path%22%3A%22%2F%22%2C%22session%22%3A15724800%2C%22end%22%3A15724800%7D%7D; pbw=%24b%3d16810%3b%24o%3d99999%3b%24sw%3d1920%3b%24sh%3d1200; pid=5783519088968567; pdomid=25; lcsrd=2020-05-28T15:52:07.2681151Z; xtide=%5B%5D; csync=0:6795618564280098310|22:1279016411279718533|25:9e445e8b-030a-4e00-beea-bc58dadfe37a|31:221d8b7c-2b37-4315-8675-10648b9f8cbc|32:3390512259795081362|33:XosAbLlQJbYAAGzcJwcAAABD&1867|49:6812547416910067852|66:04a72005004f2ea072313f57|68:no-consent|69:DYNH4sIAAAAAAAEAzO2MIk3TbVIMjAzNkq2NElKBgCHFt21EQAAAA**|75:5267d334-5a33-427a-9d14-c6563b291886|76:CAESEMy3ayQbjwwftTjg5O2M4AA|79:b23fd475-6cae-43d7-a8b0-db2f55b7e9e2|80:YMMnxmWVLJx4lnnCZcIykTeXLMZ4nynCNsKoHplB|86:864307647773797066|91:2028B818-AA01-46F4-87B3-ABB515E36B36|92:Ozx85NYvtD4v|94:XosDCwAAALja3Q9x|96:ccd4655c-4b47-4dde-a2e2-6d248152ea70|100:464a2ee1-0ce1-4962-b355-65516773da45|101:oBH0hixwXmmZXApr0vwre-jonNDvKtHp-n6iD5U4b5g=|107:22f3b795-58a0-4897-8540-5056ae39bbf8-tuct5c960ff|113:OPTOUT|116:mCMaM4A4Zv-OHL8wHDmS|117:a3e269d36cfc54929cc60d40e17d0edf|124:d4dfdf00-e8f4-41df-9c17-3db1a0fcb253|127:AAHDYE69Fg8AABClF-uFEQ|130:5f2418bbe5b48a2639303b116d6d2a3cc4b1a90f; cookieSearch-1="/venta-viviendas/valencia/extramurs/la-petxina/:1591258436314"; Trk0=Value=253245&Creation=04%2f06%2f2020+10%3a13%3a57; cto_lwid=8838753d-f687-4237-ba87-25b1dbbaf99d; atuserid=%7B%22name%22%3A%22atuserid%22%2C%22val%22%3A%22e10df39c-fe2f-4b4f-8c16-724d1a1f5b43%22%2C%22options%22%3A%7B%22end%22%3A%222021-07-07T10%3A12%3A12.231Z%22%2C%22path%22%3A%22%2F%22%7D%7D; _pxvid=c912e491-a05e-11ea-8960-c3332df5bdc9; vs=33114=3906043; xtvrn=$352991$; xtor352991=epr-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605195058; xtdate352991=442116.69919722225; cc=eyJhbGciOiJIUzI1NiJ9.eyJjdCI6ODI1Njc5NSwiZXhwIjoxNTkxNzQwODM2fQ.Eb0ayJJ4msK-ru2qfwcmxr8o6aSbbrgiktENRAlGVi4; contact516e3023-7199-4b40-86ea-b79d768d62a3="{'email':'u+PAHwQjPIOjhsVSoZjFCw==','phone':null,'phonePrefix':null,'friendEmails':null,'name':'DadAjxOHMqoKAV4aIBbTfdqUKS4BMJK2','message':null,'message2Friends':null,'maxNumberContactsAllow':10,'defaultMessage':true}"; send516e3023-7199-4b40-86ea-b79d768d62a3="{'friendsEmail':null,'email':'u+PAHwQjPIOjhsVSoZjFCw==','message':null}"; _hjAbsoluteSessionInProgress=1; xtocl352991=%24epr-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605195058%24epr-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605205456%24epr-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605184450%24epr-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605205708%24epr-201-%5Bexpress_alerts_20200606%5D-20200606-%5Bproperty-new%5D-%5B%5D-20200606132751%24epr-201-%5Bexpress_alerts_20200608%5D-20200608-%5Bproperty-new%5D-%5B%5D-20200608113036%24epr-717-%5Bexpress_alerts_20200608%5D-20200608-%5Bproperty-lowprice%5D-%5B%5D-20200608130918%24; atsession=%7B%22name%22%3A%22atsession%22%2C%22val%22%3A%7B%22histo_camp%22%3A%5B%22EPR-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605195058%22%2C%22EPR-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605205456%22%2C%22EPR-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605184450%22%2C%22EPR-201-%5Bexpress_alerts_20200605%5D-20200605-%5Bproperty-new%5D-%5B%5D-20200605205708%22%2C%22EPR-201-%5Bexpress_alerts_20200606%5D-20200606-%5Bproperty-new%5D-%5B%5D-20200606132751%22%2C%22EPR-201-%5Bexpress_alerts_20200608%5D-20200608-%5Bproperty-new%5D-%5B%5D-20200608113036%22%2C%22EPR-717-%5Bexpress_alerts_20200608%5D-20200608-%5Bproperty-lowPrice%5D-%5B%5D-20200608130918%22%5D%7D%2C%22options%22%3A%7B%22path%22%3A%22%2F%22%2C%22session%22%3A1800%2C%22end%22%3A1800%7D%7D; atreman=%7B%22name%22%3A%22atreman%22%2C%22val%22%3A%7B%22camp%22%3A%22EPR-717-%5Bexpress_alerts_20200608%5D-20200608-%5Bproperty-lowPrice%5D-%5B%5D-20200608130918%22%2C%22date%22%3A442126.46254277776%7D%2C%22options%22%3A%7B%22path%22%3A%22%2F%22%2C%22session%22%3A2592000%2C%22end%22%3A2592000%7D%7D; ABTasty=uid=by2re0mzy1nxtxnm&fst=1590680445964&pst=1591616578298&cst=1591654436806&ns=19&pvt=1134&pvis=1134&th=; _pxff_rf=1; utag_main=v_id:01714f098afa0011967a35fbd6b30206800f50600121e$_sn:91$_se:33$_ss:0$_st:1591657446162$dc_visit:72$ses_id:1591654437108%3Bexp-session$_pn:9%3Bexp-session$_prevVtSource:directTraffic%3Bexp-1591658037113$_prevVtCampaignCode:%3Bexp-1591658037113$_prevVtDomainReferrer:%3Bexp-1591658037113$_prevVtSubdomaninReferrer:%3Bexp-1591658037113$_prevVtUrlReferrer:%3Bexp-1591658037113$_prevVtprevPageName:11%3A%3Adetalle%3A%3Ahome%3Bexp-1591659246163; _px2=eyJ1IjoiMjkxNzNiYzAtYTlkOC0xMWVhLWE4M2EtYWI5NDg1ODU0OGZkIiwidiI6ImM5MTJlNDkxLWEwNWUtMTFlYS04OTYwLWMzMzMyZGY1YmRjOSIsInQiOjE1OTE2NTU5NDYzODUsImgiOiJjM2M5ZGUzNGZhZmE5ZGQxZDg5YjdhYWVmYWVkN2EyZWIyZDViYmVjY2E3OWQzN2U4OTY2NWVkZTQ3NWFkZDczIn0=; ABTastySession=mrasn=&lp=https://www.idealista.com/inmueble/89795810/&sen=10`
const userAgent =
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 11.13; rv:62.1) Gecko/20100101 Firefox/62.0"

function cleanImage(url) {
  return url
    .replace(",WEB_DETAIL_TOP", "")
    .replace("WEB_DETAIL_TOP", "WEB_DETAIL_TOP-XL-P")
    .replace(",WEB_DETAIL", "")
    .replace("WEB_DETAIL", "WEB_DETAIL_TOP-XL-P")
}

module.exports = {
  validate: async url => {
    const regex = /(https:\/\/www\.idealista\.com\/inmueble\/)(\d+)(\/)/
    const matches = url.match(regex)

    if (matches && matches.length) {
      return matches[0]
    }
  },

  /**
   * Fetch the Idealista URL
   *
   * @returns {Promise}
   */
  fetch: async url => {
    let body

    try {
      let response = await got(url, {
        headers: { "User-Agent": userAgent, Cookie: initialCookie }
      })
      return response.body
    } catch (error) {
      if (error.statusCode == 404) {
        throw new NotFoundError()
      } else if (error.statusCode == 403) {
        throw new ForbiddenError()
      }
      throw error
    }
  },

  parse: async body => {
    const $ = cheerio.load(body)

    let name = $("#headerMap li")
      .map((i, el) =>
        $(el)
          .text()
          .trim()
      )
      .get()
      .filter(i => i != "" && i != "València")
      .join(", ")
      .replace("Distrito ", "")
      .replace("Barrio ", "")
      .replace(", València", "")

    let price = $(".info-data-price > span").text()
    price = parseInt(price ? price.replace(".", "") : "")

    let m2 = parseInt($(".info-features > span:nth-child(1) > span").text())
    let bedrooms = parseInt(
      $(".info-features > span:nth-child(2) > span").text()
    )
    // let bathrooms = parseInt($('.info-features > span:nth-child(2) >
    // span').text())
    let floor = parseInt($(".info-features > span:nth-child(3) > span").text())
    let phoneNumber = $("._browserPhone")
      .text()
      .trim()

    const photosRegex = /imageDataService:"([^"]+)"/g

    let photos = []
    let match
    do {
      match = photosRegex.exec(body.toString())
      if (match) {
        photos.push(cleanImage(match[1]))
      }
    } while (match)

    // Last image is invalid
    photos.pop()

    return {
      name,
      price,
      m2,
      bedrooms,
      // bathrooms,
      floor,
      phoneNumber,
      photos
    }
  }
}
