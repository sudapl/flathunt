const Airtable = require("airtable")

module.exports = (apiKey, baseId) => {
  let base = new Airtable({ apiKey }).base(baseId)

  return {
    /**
     * Insert a record into Apartments base
     *
     * @async
     * @param {string} url Apartment URL
     * @param {object} data Apartment data
     */
    insert: (url, data) => {
      return base("Apartments").create({
        Name: data.name,
        Price: data.price,
        m2: data.m2,
        "🛌": data.bedrooms,
        Floor: data.floor,
        Phone: data.phoneNumber,
        Photos: data.photos.map(photo => {
          return {
            url: photo
          }
        }),
        Link: url
      })
    },

    /**
     * Get all available records
     *
     * @async
     */
    getAvailable: async () => {
      return base("Apartments")
        .select({
          fields: ["Link"],
          filterByFormula: "{Gone} = ''"
        })
        .all()
    },

    update: async (id, fields) => {
      try {
        await base("Apartments").update([{ id, fields }])  
      } catch (error) {
        console.log(error)
      }
      
    }
  }
}
