function UnknownUrlError(message) {
  this.name = "UnknownUrlError"
  this.message = message || ""
}
UnknownUrlError.is = error => error.name === UnknownUrlError.name

UnknownUrlError.prototype = Error.prototype
module.exports = UnknownUrlError
