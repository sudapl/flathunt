function NotFoundError(message) {
  this.name = "NotFoundError"
  this.message = message || ""
}
NotFoundError.is = error => error.name === NotFoundError.name

NotFoundError.prototype = Error.prototype
module.exports = NotFoundError
