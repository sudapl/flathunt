function ForbiddenError(message) {
  this.name = "ForbiddenError"
  this.message = message || ""
}
ForbiddenError.is = error => error.name === ForbiddenError.name

ForbiddenError.prototype = Error.prototype
module.exports = ForbiddenError
