require("dotenv").config()

const airtable = require("../airtable")(process.env.AIRTABLE_API_KEY, process.env.AIRTABLE_BASE_ID)
const parse = require("../parse")
const NotFoundError = require("../errors/NotFoundError")
const ForbiddenError = require("../errors/ForbiddenError")

async function main() {
  const available = await airtable.getAvailable()
  for (const flat of available) {
    try {
      await parse(flat.get("Link"))
      await new Promise(resolve => setTimeout(resolve, 5000))
    } catch (error) {
      if (NotFoundError.is(error)) {
        console.log(`💀 ${flat.get("Link")} is gone!`)
        try {
          await airtable.update(flat.id, {
            Gone: true
          })
        } catch (error) {
          console.log(error)
        }
        
        console.log(result)
      } else if (ForbiddenError.is(error)) {
        console.log(`⛔️ Forbidden! Probably stopped by captcha. Please check the cookies`)
        process.exit(-1)
      }
    }
  }
}

main()
