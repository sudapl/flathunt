require("dotenv").config()

const path = require("path")

const telegram = require("./telegram")(process.env.TELEGRAM_TOKEN)
const airtable = require("./airtable")(process.env.AIRTABLE_API_KEY, process.env.AIRTABLE_BASE_ID)
const parse = require("./parse")
const UnknownUrlError = require("./errors/UnknownUrlError")

function respondWithText(msg, text) {
  telegram.sendMessage(msg.chat.id, text, {
    reply_to_message_id: msg.message_id,
    parse_mode: "Markdown"
  })
}

function cleanStacktrace(error) {
  const dirPrefix = `${__dirname}${path.sep}`
  let stacktrace = error.stack ? error.stack.toString() : error.toString()
  do {
    stacktrace = stacktrace.replace(dirPrefix, "")
  } while (stacktrace.indexOf(dirPrefix) > -1)
  return stacktrace
}

telegram.on("message", async msg => {
  const chatId = msg.chat.id

  await telegram.sendChatAction(chatId, "typing")
  try {
    const data = await parse(msg.text)
    await airtable.insert(data.url, data)

    await telegram.sendPhoto(chatId, data.photos[0], {
      caption: `
        👌🏻 *${data.name}*\n
        _€${data.price / 1000}k · ${data.m2}m² · ${data.bedrooms} bedroom(s)_
      `,
      reply_to_message_id: msg.message_id,
      parse_mode: "Markdown"
    })
  } catch (error) {
    if (error instanceof UnknownUrlError) {
      respondWithText(
        msg,
        `🤷‍♀️ *Yeah... I don't understand this...* Please send me an Idealista or Fotocasa URL to fetch`
      )
    } else {
      respondWithText(msg, `😔 *Failed to fetch the URL.* Reason:\n\`${cleanStacktrace(error)}\``)
    }
  }
})
