FROM node:lts-alpine

ENV NODE_ENV=production
WORKDIR /app
ADD . /app
RUN npm install

CMD [ "npm", "start" ]