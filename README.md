# Flat Hunt Bot

A small app that receives Telegram messages containing link to property on [a supported website](#supported-real-estate-websites) it parses it and stores in [Airtable](https://airtable.com/invite/r/pv7RLgZP).

![](docs/telegram.png)

![](docs/airtable.png)

## Supported real estate websites

Currently the bot only supports two Spanish websites:

* [Idealista](https://www.idealista.com/)
* [Fotocasa](https://www.fotocasa.es/en/)

## Setup

### Telegram

1. Follow instructions by [Soham Kamani](https://www.sohamkamani.com/blog/2016/09/21/making-a-telegram-bot/#set-up-your-bot) to set up a Telegram bot (note the bot API token.).
2. Note the Telegram usernames you want to give access to

### [Airtable](https://airtable.com/invite/r/pv7RLgZP)

1. Use the [Valencia Apartment Hunting](https://airtable.com/universe/expLMcvCUNgyNeAg0/valencia-apartment-hunting) template and copy it into your own account
2. Note the API key from [your account page](https://airtable.com/account)

**Note:** currenlty the template only contains information about districts and neighborhoods for Valencia, Spain. For other cities, you might need to add them yourself.

### Deploying

The default way of deployment is using Docker. When running the container, you need to pass following environtment variables:

```
TELEGRAM_TOKEN=<TELEGRAM_TOKEN>
TELEGRAM_USERS=<COMMA_SEPARATED_USERNAMES>
AIRTABLE_BASE_ID=<AIRTABLE_BASE_ID>
AIRTABLE_API_KEY=<AIRTABLE_API_KEY>
```